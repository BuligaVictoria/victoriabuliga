package mostenire;

public abstract class Om implements ZiDeNastere{
    String  nume;
    int varsta;


    public Om() {
        nume="Nume";
        varsta=20;
    }

    public Om(String nume, int varsta) {
        this.nume=nume;
        this.varsta=varsta;
    }

    void Afisare() {
        System.out.println(nume);
        System.out.println(varsta);
    }

    abstract void Salutare();//clasa neabstracta nu poate contine clasa abstracta

    public void sarbatoreste() {
        nume = "?";
        varsta++;
    }

}
