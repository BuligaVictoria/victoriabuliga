package mostenire;

public interface FlowerShop {
    public void sellFlowers();
    public void decorateRooms();
}
