package mostenire;

public abstract class Personaj {
    String name;

    public Personaj(){
        name="Nume";
    }
    public Personaj(String name){
        this.name=name;
    }
    public void prezentare() {
        System.out.println("Eu sunt un personaj");
    }
    public void attack(){
        System.out.println("Personajul stie sa atace");
    }
}
