package mostenire;

public class Lactate implements InfoProduse {
    String denumire;
    int pret;

    public Lactate(String denumire, int pret){
        this.denumire=denumire;
        this.pret=pret;

    }
    public Lactate(){
        this.denumire="Lapte ";
        this.pret=10;
    }

    public void Livrare(){
        System.out.println(denumire +" a fost livrat la magazin ");
    }

    public void Reducere(){
        System.out.println(denumire + " este la reducere");
    }

    public static void main(String[] args) {
        Lactate lacatate= new Lactate("Smanatna",25);
        lacatate.Livrare();
        lacatate.Reducere();
        Lactate lacatate1= new Lactate();
        lacatate1.Livrare();
        lacatate1.Reducere();


    }
}
