package mostenire;

public interface Restaurant extends CoffeeShop{
    public void serveFood();
    public void organizeEvents();
}
