package mostenire;

public class Monster extends Personaj{
    int powerLevel;
   public Monster(){
        super("Fobos");
        powerLevel=89;
    }
    public Monster(int powerLevel){
        super("Snape");
        this.powerLevel=powerLevel;
    }
    @Override
    public void prezentare() {
        System.out.println("Eu sunt un monstru " + name);
    }
    @Override
    public void attack(){
       if(this.powerLevel>76){
           System.out.println(name + " este un bun atacator ");
       } else System.out.println(name+ " nu este un atacator");
    }

    public static void main(String[] args) {
        Monster monster1=new Monster();
        monster1.prezentare();
        monster1.attack();
        Monster monster2=new Monster(70);
        monster2.prezentare();
        monster2.attack();



    }
}

