public class Numar {
    public static void main(String[] args) {

        //afisarea numerelor de la 1 la n ,(n=17)

        int n=17;
        System.out.println("numerele de la 1 la 17");
        for (int i=1; i<=n;++i){
            System.out.print( " "+i);
       }
        System.out.println(" ");

       //suma numerelor de la 5 la 20

        int m=20;
        System.out.println("suma numerelor de la 5 pana la 20");
        for (int i=5; i<m;++i) {
            System.out.print( " " + (i + m));
        }
    }
}
